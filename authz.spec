%global debug_package %{nil}

Name:           authz
Version:        0.1
Release:        2
Summary:        a isula auth plugin for RBAC
License:        Mulan PSL v2
URL:            https://gitee.com/openeuler/authz
Source0:        https://gitee.com/openeuler/authz/repository/archive/v%{version}.tar.gz
BuildRoot:      %{_tmppath}/authz-root

#Dependency
BuildRequires: golang >= 1.8
BuildRequires: glibc-static

%description
Work with isulad daemon that enables TLS. It brings the support of RBAC.

#Build sections
%prep
%setup -n %{name} -q

%build
make

%install
mkdir -p $RPM_BUILD_ROOT/usr/lib/isulad/
mkdir -p $RPM_BUILD_ROOT/lib/systemd/system/
mkdir -p $RPM_BUILD_ROOT/var/lib/authz-broker/

cp bin/authz-broker $RPM_BUILD_ROOT/usr/lib/isulad/
cp systemd/authz.service $RPM_BUILD_ROOT/lib/systemd/system/

chmod 0750 $RPM_BUILD_ROOT/usr/lib/isulad/authz-broker

#Install and uninstall scripts
%pre

%preun
%systemd_preun authz

%post
if [ ! -d "/var/lib/authz-broker" ]; then
	mkdir -p /var/lib/authz-broker
fi
chmod 0750 /var/lib/authz-broker
if [ ! -f "/var/lib/authz-broker/policy.json" ]; then
	cat > /var/lib/authz-broker/policy.json << EOF
{"name":"policy_root","users":[""],"actions":[""]}
EOF
fi
chmod 0640 /var/lib/authz-broker/policy.json

%postun

#Files list
%files
%attr(550,root,root) /usr/lib/isulad
%attr(550,root,root) /usr/lib/isulad/authz-broker
%attr(640,root,root) /lib/systemd/system/authz.service

#Clean section
%clean
rm -rfv %{buildroot}

%changelog
* Mon Sep 07 2020 wangkang101 <873229877@qq.com> - 0.1-2
- modify url of source0

* Fri Jul 03 2020 zhangsong234 <zhangsong34@huawei.com> - 0.1-1
- release version 0.1
